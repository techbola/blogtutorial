<?php

Route::get('/', function () {
    return view('blog.index');
});

Route::get('/post/show', function () {
    return view('blog.show');
});